#include <stdio.h>
#include <stdlib.h>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */
  
int fibonacciSeq(int);

int fibonacciSeq(int n){
	if (n==0) return 0;
	else if (n==1) return 1;
	else return fibonacciSeq(n-1)+fibonacciSeq(n-2);
	
}
int main() {
	int nvalue,i;
	printf("Enter The number(n) for the Fibonnaci Sequence: ");
	scanf("%d",&nvalue);
	
	
	for(i = 0;i<=nvalue;i++) {
      printf("%d \n",fibonacciSeq(i));            
   }
	return 0;
}
